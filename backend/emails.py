from flask_mail import Mail, Message

mail = Mail()

def login_email(email, token):
    msg = Message(
        subject='Log in to Schedule MD',
        recipients=[email],
        body="""Use this token to log in to Schedule MD: {}

# It will expire in 15 minutes, but don't worry -- you can always get another one :)""".format(token.decode('utf-8')))
    mail.send(msg)

def init_app(app):
    mail.init_app(app)
