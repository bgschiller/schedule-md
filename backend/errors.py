class ApiError(Exception):
    def to_dict(self):
        return {
            'message': str(self),
            'error_type': self.__class__.__name__,
        }
    status_code = 400

class BadTokenError(ApiError):
    def __init__(self):
        super().__init__()
        self.message = 'Received an invalid token'
    status_code = 401

class ExpiredTokenError(BadTokenError):
    def __init__(self):
        super().__init__()
        self.message = 'Received an expired token'

class NotLoggedInError(ApiError):
    def __init__(self):
        super().__init__()
        self.message = 'You must be logged in to do that'
    status_code = 401

class NotAuthorizedError(ApiError):
    def __init__(self):
        super().__init__()
        self.message = "You don't have authority to do that"
    status_code = 401

class MissingParameterError(ApiError):
    status_code = 400
