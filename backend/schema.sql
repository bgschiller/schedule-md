CREATE TABLE users (
    id serial primary key,
    name varchar(80),
    email varchar(100) NOT NULL UNIQUE
);

CREATE TABLE schedules (
    id serial primary key,
    owner_id integer references users,
    name varchar(100),
    blocks_status varchar(35) default 'unstarted',
    rotations_status varchar(35) default 'unstarted',
    residents_status varchar(35) default 'unstarted',
    program_constraints_status varchar(35) default 'unstarted',
    created_at timestamp default current_timestamp
);

CREATE TABLE rotations (
    id serial primary key,
    schedule_id integer references schedules,
    name varchar(80),
    tags varchar[]
);

CREATE TABLE blocks (
    id serial primary key,
    schedule_id integer references schedules,
    start_day date,
    end_day date,
    name varchar(80)
);

CREATE TABLE staffing_requirements (
    id serial primary key,
    schedule_id integer references schedules,
    rotation_id integer references rotations,
    block_id integer references blocks,
    at_most integer,
    at_least integer default 0
);

CREATE TABLE residents (
    id serial primary key,
    user_id integer references users,
    schedule_id integer references schedules,
    UNIQUE(user_id, schedule_id),
    tags varchar[]
);

CREATE TABLE resident_preferences (
    id serial primary key,
    resident_id integer references residents,
    position integer,
    options jsonb
);

CREATE TABLE program_preferences (
    id serial primary key,
    schedule_id integer references schedules,
    options jsonb
);
