from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.ext.automap import automap_base

db = SQLAlchemy()
db.Model = automap_base(db.Model)

class User(db.Model):
    __tablename__ = 'users'

class Schedule(db.Model):
    __tablename__ = 'schedules'

class Rotation(db.Model):
    __tablename__ = 'rotations'

class Block(db.Model):
    __tablename__ = 'blocks'

class StaffingRequirement(db.Model):
    __tablename__ = 'staffing_requirements'

class Resident(db.Model):
    __tablename__ = 'residents'

class ResidentPreference(db.Model):
    __tablename__ = 'resident_preferences'

class ProgramPreference(db.Model):
    __tablename__ = 'program_preferences'

def name_for_collection_relationship(base, local_cls, referred_cls, constraint):
    """
    Specify how collections on model instances should be named.
    Ex:
    r = Resident.query.first()
    r.resident_preferences <-- that name there

    We'll just use the __tablename__ attribute, since it's plural.

    Possible that we'll have to change it in the future if we have multiple
    relationships between the same two tables.
    """
    return referred_cls.__tablename__

def name_for_scalar_relationship(base, local_cls, referred_cls, constraint):
    """
    Specify how scalars on model instances should be named.
    Ex:
    r = Resident.query.first()
    r.user <-- that name there

    We'll use the __tablename__ with the final 's' dropped. This works for now,
    but we might be adding overrides in the future, since plurals aren't always
    consistent. The price of a nice API *shrug*
    """
    return referred_cls.__tablename__[:-1]

def init_app(app):
    db.init_app(app)
        
    with app.app_context():
        db.Model.prepare(
            db.engine,
            reflect=True,
            name_for_collection_relationship=name_for_collection_relationship,
            name_for_scalar_relationship=name_for_scalar_relationship,
        )
