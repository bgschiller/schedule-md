from sqlalchemy.sql import text
from .models import db

def dictfetchall(query, params):
    result = db.engine.execute(text(query), params)
    ks = result.keys()
    return [dict(zip(ks, row)) for row in result]

def dictfetchone(*args, **kwargs):
    rows = dictfetchall(*args, **kwargs)
    if not rows:
        return None
    return rows[0]
