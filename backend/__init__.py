import os

from flask import Flask
def create_app(test_config=None):
    app = Flask(__name__)

    app.config.from_pyfile('config.py', silent=True)
    if test_config is not None:
        app.config.update(test_config)


    from .models import init_app
    init_app(app)
    from .emails import init_app
    init_app(app)

    from .routes import bp
    app.register_blueprint(bp, url_prefix='/api')

    return app