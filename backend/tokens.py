import os
from itsdangerous import TimestampSigner, BadSignature, SignatureExpired
from .errors import BadTokenError, ExpiredTokenError

SECRET_KEY = os.getenv('FLASK_SECRET_KEY', 'moosefeathers')

notary = TimestampSigner(SECRET_KEY)

def login_token(email):
    return notary.sign(email)

FIFTEEN_MINUTES = 15 * 60
def validate_login_token(token, max_age_seconds=FIFTEEN_MINUTES):
    try:
        return notary.unsign(token, max_age=max_age_seconds).decode('utf-8')
    except SignatureExpired:
        raise ExpiredTokenError()
    except BadSignature:
        raise BadTokenError()
