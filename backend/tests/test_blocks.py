from datetime import date
from ..routes.blocks import block_title, BlockLength


def test_block_titles():
    assert block_title(date(2018, 3, 1), date(2018, 3, 31), BlockLength.MONTH) == 'Mar'
    assert (
        block_title(date(2018, 3, 1), date(2018, 4, 12), BlockLength.WEEKS_6)
        == 'early Mar to mid Apr')
    assert block_title(date(2018, 3, 5), date(2018, 4, 4), BlockLength.DAYS_30) == 'early Mar'
    assert block_title(date(2018, 3, 15), date(2018, 4, 11), BlockLength.DAYS_28) == 'mid Mar'
