from ..models import Schedule

def test_cannot_create_without_login(client):
    rv = client.post('/api/schedule', json=dict(name='My first schedule'))
    assert rv.status_code == 401

def test_create_schedule_sets_owner(client):
    with client.session_transaction() as session:
        session['user_id'] = 2
    rv = client.post('/api/schedule', json=dict(name='My first schedule'))
    assert rv.status_code == 200
    data = rv.get_json()
    assert 'id' in data
    s = Schedule.query.get(data['id'])
    assert s.owner_id == 2

def test_schedule_starts_with_unstarted_statuses(client):
    with client.session_transaction() as session:
        session['user_id'] = 2
    rv = client.post('/api/schedule', json=dict(name='My second schedule'))
    assert rv.status_code == 200
    data = rv.get_json()
    s = Schedule.query.get(data['id'])
    assert s.blocks_status == 'unstarted'
    assert s.rotations_status == 'unstarted'
    assert s.residents_status == 'unstarted'
    assert s.program_constraints_status == 'unstarted'

def test_schedule_cannot_be_updated_if_not_logged_in(client):
    rv = client.put('/api/schedule/1', json=dict(blocks_status='in-progress'))
    assert rv.status_code == 401

def test_schedule_may_not_be_updated_if_not_owner(client):
    with client.session_transaction() as session:
        session['user_id'] = 2
    rv = client.put('/api/schedule/1', json=dict(name='this isnt mine'))
    assert rv.status_code == 401

def test_schedule_status_may_only_be_set_to_allowed_value(client):
    with client.session_transaction() as session:
        session['user_id'] = 1
    for status_key in ('blocks_status', 'rotations_status', 'residents_status', 'program_constraints_status'):
        rv = client.put('/api/schedule/1', json={ status_key: 'negatory' })
        assert rv.status_code == 400

def test_schedule_id_cannot_be_changed(client):
    with client.session_transaction() as session:
        session['user_id'] = 1
    rv = client.put('/api/schedule/1', json=dict(id=12))
    assert rv.status_code == 400

def test_schedule_owner_cannot_be_changed(client):
    with client.session_transaction() as session:
        session['user_id'] = 1
    rv = client.put('/api/schedule/1', json=dict(owner_id=14))
    assert rv.status_code == 400


def test_unauthed_user_cannot_see_schedule(client):
    rv = client.get('/api/schedule/1')
    assert rv.status_code == 401

def test_schedule_owner_can_see_detail(client):
    with client.session_transaction() as session:
        session['user_id'] = 1
    rv = client.get('/api/schedule/1')
    assert rv.status_code == 200

def test_resident_on_schedule_may_see_it(client):
    with client.session_transaction() as session:
        session['user_id'] = 2
    rv = client.get('/api/schedule/1')
    assert rv.status_code == 200

def test_schedule_detail_contains_necessary_keys(client):
    with client.session_transaction() as session:
        session['user_id'] = 2
    rv = client.get('/api/schedule/1')
    assert rv.status_code == 200
    data = rv.get_json()

    schedule_items = (
        'name', 'blocks', 'rotations', 'residents',
        'staffing_requirements',
    )
    for required_key in schedule_items:
        assert required_key in data

    rotation_names = {r['name'] for r in data['rotations']}
    assert {'Nap on Couch', 'Nap on Bed', 'Snuggle on Couch'} == rotation_names

    block_names = { b['name'] for b in data['blocks'] }
    assert {'January', 'February', 'March'} == block_names
