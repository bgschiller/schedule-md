from ..emails import login_email, mail

def test_login_email():
    """
    it should contain the token, and be directed at the email
    """
    with mail.record_messages() as outbox:
        login_email('student@example.com', b'this-is-a-token')

        assert len(outbox) == 1
        assert outbox[0].recipients == ['student@example.com']
        assert 'this-is-a-token' in outbox[0].body

def test_login_email_contains_valid_token_works(client):
    """
    The token in an email should be suitable for logging in.
    """
    email = 'test@example.com'
    with mail.record_messages() as outbox:
        rv = client.post(
            '/api/send_login_email',
            json=dict(email=email),
        )
        assert rv.status_code == 200

        assert len(outbox) == 1
        token_email = outbox[0].body
        token_prefix = 'Use this token to log in to Schedule MD: '
        prefix_loc = token_email.index(token_prefix)
        end_loc = token_email.index('\n', prefix_loc)
        token = token_email[prefix_loc + len(token_prefix):end_loc].strip()
        assert token

        rv = client.post(
            '/api/login',
            json=dict(token=token),
        )
        assert rv.status_code == 200
        data = rv.get_json()
        assert data['email'] == email
