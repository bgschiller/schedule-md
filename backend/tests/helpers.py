import string
from hypothesis import strategies

def combine_email(username, domain, tld):
    return '{}@{}.{}'.format(username, domain, tld)

email_strategy = strategies.builds(
    combine_email,
    strategies.text(string.ascii_lowercase, max_size=60),
    strategies.text(string.ascii_letters + '.', max_size=30),
    strategies.text(string.ascii_letters + '.', max_size=5))
