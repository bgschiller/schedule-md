from ..tokens import login_token
from ..models import User

def test_existing_user_login_finds_same_user(client):
    email = 'brian@brian.com'
    brian = User.query.filter_by(email=email).first()
    assert brian is not None
    token = login_token(email)
    rv = client.post(
        '/api/login',
        json=dict(token=token.decode('utf-8')),
    )
    assert rv.status_code == 200
    data = rv.get_json()
    assert data['id'] == brian.id

def test_new_user_creates_db_record(client):
    email = 'new@new.com'
    user = User.query.filter_by(email=email).first()
    assert user is None
    token = login_token(email)
    rv = client.post(
        '/api/login',
        json=dict(token=token.decode('utf-8')),
    )
    assert rv.status_code == 200
    data = rv.get_json()
    assert data['id'] is not None
