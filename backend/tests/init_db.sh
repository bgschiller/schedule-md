#!/usr/bin/env bash
set -e

if [[ ${CI} ]]; then
    HOST=postgres
    USER=brian
else
    psql -c "DROP DATABASE IF EXISTS schedule_md_test"
    createdb schedule_md_test
    HOST=localhost
    USER=postgres
fi

echo "HOST IS ${HOST}. USER IS ${USER}"

psql --username=${USER} --host=${HOST} -d schedule_md_test < ./schema.sql
psql --username=${USER} --host=${HOST} -d schedule_md_test < ./stub.sql