import pytest
import datetime
from freezegun import freeze_time
from hypothesis import given, settings

from ..tokens import login_token, validate_login_token
from ..errors import ExpiredTokenError, BadTokenError
from .helpers import email_strategy

@given(email_strategy)
def test_roundtrip_validation(email):
    token = login_token(email)
    validated = validate_login_token(token)
    assert validated == email

@given(email=email_strategy)
@settings(max_examples=5, deadline=None)  # this test takes a long time. HMAC, y'know.
def test_expired_fails(email):
    with freeze_time() as frozen:
        token = login_token(email)

        frozen.tick(delta=datetime.timedelta(minutes=5))
        validate_login_token(token)

        frozen.tick(delta=datetime.timedelta(minutes=16))
        with pytest.raises(ExpiredTokenError):
            validate_login_token(token)

@given(email_strategy)
def test_bad_token_is_bad(email):
    with pytest.raises(BadTokenError):
        validate_login_token('not really a token')
