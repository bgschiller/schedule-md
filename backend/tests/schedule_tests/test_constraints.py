import itertools
import citrus
import pulp
from schedule.constraints import (
    no_time_turners, at_least_x_blocks_of_rotation, at_most_x_blocks_of_rotation,
    exactly_x_blocks_of_rotation,
)

def test_no_time_turners(pieces):
    p = citrus.Problem('no time turners', pulp.LpMaximize)

    X = p.dicts(
        'x',
        pieces.vars(),
        cat=pulp.LpBinary,
    )
    # set weak prior for all variables
    p.setObjective(sum(0.001 * X[v] for v in pieces.vars()))

    no_time_turners(p, X, pieces)

    p.solve()
    assert pulp.LpStatus[p.status] == 'Optimal'

    for b, res in itertools.product(pieces.blocks, pieces.residents):
        assert sum(X[b, rot, res].value() for rot in pieces.rotations) == 1

def test_exactly_x_blocks_of(pieces):
    p = citrus.Problem('exactly x blocks of', pulp.LpMaximize)

    X = p.dicts(
        'x',
        pieces.vars(),
        cat=pulp.LpBinary
    )

    # set weak prior to never do anything
    p.setObjective(sum(-0.001 * X[v] for v in pieces.vars()))

    exactly_x_blocks_of_rotation(p, X, pieces, 'a', 3)
    exactly_x_blocks_of_rotation(p, X, pieces, 'b', 3)
    exactly_x_blocks_of_rotation(p, X, pieces, 'c', 1)

    p.solve()
    assert pulp.LpStatus[p.status] == 'Optimal'

    for r in pieces.residents:
        assert sum(X[b, 'a', r] for b in pieces.blocks) == 3
        assert sum(X[b, 'b', r] for b in pieces.blocks) == 3
        assert sum(X[b, 'c', r] for b in pieces.blocks) == 1

def test_at_most_and_least_x_blocks_of(pieces):
    p = citrus.Problem('at most x blocks of', pulp.LpMaximize)

    X = p.dicts(
        'x',
        pieces.vars(),
        cat=pulp.LpBinary
    )

    p.setObjective(
        # prefer to do things
        sum(0.001 * X[v] for v in pieces.vars()) +
        # but not _b_ things!
        sum(-0.002 * X[b, rot, res] for b, rot, res in pieces.vars() if rot == 'b')
    )

    at_most_x_blocks_of_rotation(p, X, pieces, 'a', 3)
    at_most_x_blocks_of_rotation(p, X, pieces, 'b', 3)
    at_least_x_blocks_of_rotation(p, X, pieces, 'b', 1)
    at_most_x_blocks_of_rotation(p, X, pieces, 'c', 1)
    at_least_x_blocks_of_rotation(p, X, pieces, 'c', 0)

    p.solve()
    assert pulp.LpStatus[p.status] == 'Optimal'

    for r in pieces.residents:
        assert sum(X[b, 'a', r] for b in pieces.blocks) == 3
        assert sum(X[b, 'b', r] for b in pieces.blocks) == 1
        assert sum(X[b, 'c', r] for b in pieces.blocks) == 1
