import itertools
import pytest
import citrus
import pulp
from schedule.preferences.resident import XDuringY
from schedule.pieces import SchedulePieces
from schedule.errors import InvalidPreferenceError


def test_cannot_init_without_resident_id():
    with pytest.raises(TypeError):
        XDuringY(block_ids=('jan', 'feb'), rotation_ids=('a',))

def test_validate_fails_with_missing_resident_id(pieces):
    xy = XDuringY(resident_id='cody', block_ids=('jan', 'feb'), rotation_ids=('b',))
    with pytest.raises(InvalidPreferenceError):
        xy.validate(pieces)

def test_to_json_from_json_is_identity():
    xy = XDuringY(resident_id='john', rotation_tag='consonants', block_ids=('jan', 'feb'))
    obj = xy.to_json()
    assert obj == XDuringY.from_json(obj).to_json()

def test_to_affine_expression_impacts_schedule(pieces):
    p = citrus.Problem('x during y', pulp.LpMaximize)

    X = p.dicts(
        'x',
        pieces.vars(),
        cat=pulp.LpBinary,
    )

    objective = []

    # initialize with a weak prior pref to have john not work anything
    for b, rot, res in pieces.vars():
        objective.append(-0.001 * X[b, rot, res])
    
    consonants_in_the_winter = XDuringY('john', rotation_tag='consonants', block_ids=('jan', 'feb'))
    consonants_in_the_winter.validate(pieces)

    objective.append(consonants_in_the_winter.to_affine_expression(X, pieces))
    p.setObjective(sum(objective))
    p.solve()
    assert pulp.LpStatus[p.status] == 'Optimal'

    for b, rot, res in pieces.vars():
        if b in ('jan', 'feb') and rot in ('b', 'c') and res == 'john':
            assert X[b, rot, res].value() == 1.0
        else:
            assert X[b, rot, res].value() == 0.0
