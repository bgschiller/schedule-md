import itertools
import pytest
import citrus
import pulp
from ....schedule.preferences.resident import NoTwoXInARow
from ....schedule.pieces import SchedulePieces
from ....schedule.errors import InvalidPreferenceError

BLOCKS = ('jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul')
ROTATIONS = ('a', 'b', 'c')
RESIDENTS = ('john',)
RESIDENTS_BY_TAG = {}
ROTATIONS_BY_TAG = { 'consonants': ('b', 'c') }

pieces = SchedulePieces(
    blocks=BLOCKS,
    rotations=ROTATIONS,
    residents=RESIDENTS,
    rotations_by_tag=ROTATIONS_BY_TAG,
    residents_by_tag=RESIDENTS_BY_TAG,
)

def test_cannot_init_without_resident_id():
    with pytest.raises(TypeError):
        NoTwoXInARow(rotation_ids=('a',))

def test_validate_fails_with_missing_resident_id():
    no_two = NoTwoXInARow(resident_id='cody', rotation_ids=('b',))
    with pytest.raises(InvalidPreferenceError):
        no_two.validate(pieces)

def test_to_json_from_json_is_identity():
    no_two = NoTwoXInARow(resident_id='john', rotation_tag='consonants')
    obj = no_two.to_json()
    assert obj == NoTwoXInARow.from_json(obj).to_json()

def test_to_affine_expression_impacts_schedule():
    p = citrus.Problem('no two in a row', pulp.LpMaximize)

    X = p.dicts(
        'x',
        itertools.product(BLOCKS, ROTATIONS, RESIDENTS),
        cat=pulp.LpBinary,
    )

    objective = []

    # initialize with a weak prior pref to have john work everything
    for b, rot, res in itertools.product(BLOCKS, ROTATIONS, RESIDENTS):
        objective.append(0.001 * X[b, rot, res])
    
    no_consonants_in_a_row = NoTwoXInARow('john', rotation_tag='consonants')
    no_consonants_in_a_row.validate(pieces)

    objective.append(no_consonants_in_a_row.to_affine_expression(X, pieces))
    p.setObjective(sum(objective))
    p.solve()
    assert pulp.LpStatus[p.status] == 'Optimal'

    for b1, b2 in zip(BLOCKS, BLOCKS[1:]):
        b1_consonant_on = X[b1, 'b', 'john'].value() or X[b1, 'c', 'john'].value()
        b2_consonant_on = X[b2, 'b', 'john'].value() or X[b2, 'c', 'john'].value()
        assert not (b1_consonant_on and b2_consonant_on)
