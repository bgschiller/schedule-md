import itertools
import pytest
import citrus
import pulp
from schedule.preferences.resident import AsEarlyAsPossible, AsLateAsPossible
from schedule.constraints import exactly_x_blocks_of_rotation, no_time_turners
from schedule.pieces import SchedulePieces
from schedule.errors import InvalidPreferenceError

def test_cannot_init_without_resident_id():
    with pytest.raises(TypeError):
        AsEarlyAsPossible(rotation_ids=('a',))

def test_validate_fails_with_missing_resident_id(pieces):
    aeap = AsEarlyAsPossible(resident_id='cody', rotation_ids=('b',))
    with pytest.raises(InvalidPreferenceError):
        aeap.validate(pieces)

def test_to_json_from_json_is_identity():
    aeap = AsEarlyAsPossible(resident_id='john', rotation_tag='consonants')
    obj = aeap.to_json()
    assert obj == AsEarlyAsPossible.from_json(obj).to_json()

def test_to_affine_expression_impacts_schedule(pieces):
    p = citrus.Problem('as early as possible', pulp.LpMaximize)

    X = p.dicts(
        'x',
        pieces.vars(),
        cat=pulp.LpBinary,
    )

    objective = []

    aeap = AsEarlyAsPossible('john', rotation_tag='consonants')
    aeap.validate(pieces)

    alap = AsLateAsPossible('sara', rotation_tag='consonants')
    alap.validate(pieces)

    exactly_x_blocks_of_rotation(p, X, pieces, 'b', 1)
    exactly_x_blocks_of_rotation(p, X, pieces, 'c', 1)
    no_time_turners(p, X, pieces)

    objective.append(aeap.to_affine_expression(X, pieces))
    objective.append(alap.to_affine_expression(X, pieces))

    p.setObjective(sum(objective))
    p.solve()
    assert pulp.LpStatus[p.status] == 'Optimal'

    assert X['jan', 'b', 'john'].value() + X['jan', 'c', 'john'].value() == 1.0
    assert X['feb', 'b', 'john'].value() + X['feb', 'c', 'john'].value() == 1.0
    assert X['jun', 'b', 'sara'].value() + X['jun', 'c', 'sara'].value() == 1.0
    assert X['jul', 'b', 'sara'].value() + X['jul', 'c', 'sara'].value() == 1.0

    assert sum(
        X[b, rot, 'john'].value()
        for rot in ('b', 'c')
        for b in pieces.blocks
        if b not in ('jan', 'feb')
    ) == 0, "No other months' b or c is triggered"

    assert sum(
        X[b, rot, 'sara'].value()
        for rot in ('b', 'c')
        for b in pieces.blocks
        if b not in ('jun', 'jul')
    ) == 0, "No other months' b or c is triggered"