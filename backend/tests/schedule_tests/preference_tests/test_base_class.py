import pytest
from ....schedule.preferences.base import BasePreference

def test_cannot_fail_to_provide_validate():
    class NoValidate(BasePreference):
        def to_json(self):
            pass
        
        def to_affine_expression(self, X, schedule):
            pass
    with pytest.raises(TypeError):
        NoValidate()
    
def test_cannot_fail_to_provide_to_json():
    class NoToJson(BasePreference):
        def validate(self, schedule):
            pass
        def to_affine_expression(self, X, schedule):
            pass
    with pytest.raises(TypeError):
        NoToJson()

def test_cannot_fail_to_provide_affine():
    class NoAffineExpression(BasePreference):
        def to_json(self):
            pass
        def validate(self, schedule):
            pass
    with pytest.raises(TypeError):
        NoAffineExpression()