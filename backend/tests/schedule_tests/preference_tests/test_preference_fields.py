import itertools
import pytest
import citrus
import pulp
from schedule.preferences.fields import RotationChoice, ResidentChoice
from schedule.pieces import SchedulePieces
from schedule.errors import InvalidPreferenceError

def test_cannot_init_with_both_rotation_tag_and_id():
    with pytest.raises(InvalidPreferenceError):
        RotationChoice(rotation_ids=('a',), rotation_tag='consonants')

def test_cannot_init_without_one_of_rotation_tag_or_id():
    with pytest.raises(InvalidPreferenceError):
        RotationChoice()

def test_ignores_extra_kwargs():
    """
    This is useful so that Preferences don't need to know
    which kwargs to give to which field
    """
    RotationChoice(rotation_ids=('a','b',), salt='straw')

def test_validate_fails_with_missing_rotation_id(pieces):
    rc = RotationChoice(resident_id='john', rotation_ids=('d',))
    with pytest.raises(InvalidPreferenceError):
        rc.validate(pieces)

def test_validate_fails_if_rotation_tag_not_found(pieces):
    rc = RotationChoice(resident_id='john', rotation_tag='vowels')
    with pytest.raises(InvalidPreferenceError):
        rc.validate(pieces)

def test_cannot_init_with_both_resident_tag_and_id():
    with pytest.raises(InvalidPreferenceError):
        ResidentChoice(resident_ids=('john',), resident_tag='DH')

def test_cannot_init_without_one_of_resident_tag_or_id():
    with pytest.raises(InvalidPreferenceError):
        ResidentChoice()

def test_ignores_extra_kwargs():
    """
    This is useful so that Preferences don't need to know
    which kwargs to give to which field
    """
    ResidentChoice(resident_ids=('john','sara',), salt='straw')

def test_validate_fails_with_missing_resident_id(pieces):
    rc = ResidentChoice(resident_ids=('cody',))
    with pytest.raises(InvalidPreferenceError):
        rc.validate(pieces)

def test_validate_fails_if_resident_tag_not_found(pieces):
    rc = ResidentChoice(resident_tag='dogs')
    with pytest.raises(InvalidPreferenceError):
        rc.validate(pieces)
