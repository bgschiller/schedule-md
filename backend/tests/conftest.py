import os
import pytest
import subprocess
from .. import create_app
from schedule.pieces import SchedulePieces

@pytest.fixture(scope='session', autouse=True)
def app():
    subprocess.run('tests/init_db.sh', check=True)
    app = create_app({
        'TESTING': True,
        'SQLALCHEMY_DATABASE_URI': os.getenv('TEST_DATABASE_URL')
    })
    with app.app_context():
        yield app


@pytest.fixture
def client(app):
    client = app.test_client()

    yield client


@pytest.fixture(scope='session')
def pieces():
    BLOCKS = ('jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul')
    ROTATIONS = ('a', 'b', 'c')
    RESIDENTS = ('john', 'sara')

    yield SchedulePieces(
        blocks=BLOCKS,
        rotations=ROTATIONS,
        residents=RESIDENTS,
        rotations_by_tag={ 'consonants': ('b', 'c') },
        residents_by_tag={},
    )