from flask import request, jsonify, session
from .blueprint import bp
from ..errors import ApiError
from ..tokens import login_token, validate_login_token
from ..emails import login_email
from ..models import User, db

@bp.route('/send_login_email', methods=['POST'])
def send_login_email():
    email = request.get_json()['email']
    if '@' not in email:
        raise ApiError("Expected an email, but that didn't look like one...")
    token = login_token(email)
    login_email(email, token)
    return jsonify({'status': 'ok'})

@bp.route('/login', methods=['POST'])
def login():
    token = request.get_json()['token']
    email = validate_login_token(token)
    user = User.query.filter_by(email=email).first()
    if not user:
        user = User(email=email)
        db.session.add(user)
        db.session.commit()
    session['user_id'] = user.id
    return jsonify({'email': email, 'id': user.id, 'name': user.name})
