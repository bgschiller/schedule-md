import datetime
from enum import Enum
from dateutil.relativedelta import relativedelta
from dateutil.parser import parse
from flask import request, jsonify
from ..models import db, Block
from .blueprint import bp
from .auth import login_required, must_own_schedule, requires_params


class BlockLength(Enum):
    DAYS_28 = relativedelta(days=28)
    DAYS_30 = relativedelta(days=30)
    WEEKS_6 = relativedelta(weeks=6)
    MONTH = relativedelta(months=1)


def relative_month_part(day: int) -> str:
    if 1 <= day <= 8:
        return 'early'
    if 9 <= day <= 22:
        return 'mid'
    return 'late'


def block_title(start_date: datetime.date, end_date: datetime.date, length: BlockLength):
    if length == BlockLength.MONTH:
        return start_date.strftime('%b')
    if length in (BlockLength.DAYS_28, BlockLength.DAYS_30):
        return f"{relative_month_part(start_date.day)} {start_date.strftime('%b')}"
    return (
        f"{relative_month_part(start_date.day)} {start_date.strftime('%b')}" +
        " to " +
        f"{relative_month_part(end_date.day)} {end_date.strftime('%b')}"
    )


def block_dates(start: datetime.date, length: BlockLength, num: int):
    blocks = []
    curr_start = start
    delta = length.value
    one_day = relativedelta(days=1)
    while len(blocks) < num:
        next_start = start + delta
        this_end = next_start - one_day
        blocks.append({
            'start_day': curr_start,
            'end_day': this_end,
            'name': block_title(curr_start, this_end, length),
        })
    return blocks

@bp.route('/schedule/<int:schedule_id>/blocks', methods=['POST'])
@login_required
@must_own_schedule
@requires_params('start_date', 'num_blocks', 'block_length')
def create_blocks(schedule_id):
    data = request.get_json()
    block_len = BlockLength[data['block_length']]
    start_date = parse(data['start_date']).date()
    num_blocks = int(data['num_blocks'])
    block_data = block_dates(start_date, block_len, num_blocks)
    blocks = [Block(**b, schedule_id=schedule_id) for b in block_data]
    db.session.add_all(blocks)
    db.session.commit()
    return jsonify(list(map(dict, blocks)))
