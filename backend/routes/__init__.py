from flask import jsonify
from ..errors import ApiError
from .blueprint import bp

@bp.errorhandler(ApiError)
def handle_api_error(error):
    print('error', error)
    response = jsonify(error.to_dict())
    response.status_code = error.status_code or 400
    return response

from . import login
from . import schedules