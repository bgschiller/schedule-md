import inspect
from functools import wraps
from flask import session, abort, request
from ..models import Schedule, Resident
from ..errors import NotLoggedInError, NotAuthorizedError, MissingParameterError

def login_required(view):
    @wraps(view)
    def auth_checker(*args, **kwargs):
        if 'user_id' not in session:
            raise NotLoggedInError()
        return view(*args, **kwargs)
    return auth_checker


def static_ensure_schedule_id_param(view):
    signature = inspect.signature(view)
    assert 'schedule_id' in signature.parameters, "Cannot determine schedule to check"
    return signature

def find_schedule(signature, args, kwargs):
    params = signature.bind(*args, **kwargs).arguments
    schedule_id = params['schedule_id']

    return Schedule.query.get(schedule_id)

def must_own_schedule(view):
    sig = static_ensure_schedule_id_param(view)

    @wraps(view)
    def schedule_ownership_checker(*args, **kwargs):
        schedule = find_schedule(sig, args, kwargs)
        if schedule is None:
            abort(404)
        if schedule.owner_id != session['user_id']:
            raise NotAuthorizedError()
        return view(*args, **kwargs)
    return schedule_ownership_checker

def must_belong_to_schedule(view):
    sig = static_ensure_schedule_id_param(view)

    @wraps(view)
    def schedule_belong_checker(*args, **kwargs):
        schedule = find_schedule(sig, args, kwargs)
        if schedule is None:
            abort(404)
        matching_resident = (
            Resident.query
            .filter_by(user_id=session['user_id'], schedule_id=schedule.id)
            .count())
        if schedule.id != session['user_id'] and matching_resident == 0:
            raise NotAuthorizedError()
        return view(*args, **kwargs)
    return schedule_belong_checker

def requires_params(*params):
    def decorator(view):
        @wraps(view)
        def decorated(*args, **kwargs):
            data = request.get_json()
            missing = [p for p in params if not data or p not in data]
            if not missing:
                raise MissingParameterError(f"Expected values for {missing}")
            return view(*args, **kwargs)
        return decorated
    return decorator
