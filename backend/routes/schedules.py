from flask import request, session, jsonify
from ..models import db, Schedule
from ..utils import dictfetchone, dictfetchall
from .blueprint import bp
from .auth import login_required, must_own_schedule, must_belong_to_schedule

@bp.route('/schedule', methods=['POST'])
@login_required
def create_schedule():
    data = request.get_json()
    name = data['name']
    s = Schedule(name=name, owner_id=session['user_id'])
    db.session.add(s)
    db.session.commit()
    return jsonify({ 'id' : s.id })

STATUS_COLUMNS = (
    'blocks_status', 'rotations_status',
    'residents_status', 'program_constraints_status',
)
PERMITTED_STATUSES = (
    'unstarted', 'in-progress', 'completed',
)
PERMITTED_SCHEDULE_CHANGES = STATUS_COLUMNS + ('name',)
def validate_schedule_change(updates):
    errors = []
    for k, v in updates.items():
        if k not in PERMITTED_SCHEDULE_CHANGES:
            errors.append(f'"{k}" is not a permissible field to update')
        if k in STATUS_COLUMNS and v not in PERMITTED_STATUSES:
            errors.append(
             f'"{v}" is not a permissible status value')

    return errors

@bp.route('/schedule/<int:schedule_id>', methods=['PUT'])
@login_required
@must_own_schedule
def update_schedule(schedule_id):
    data = request.get_json()
    errors = validate_schedule_change(data)
    if errors:
        resp = jsonify(dict(errors=errors))
        resp.status_code = 400
        return resp
    schedule = Schedule.query.get(schedule_id)
    for k, v in data.items():
        setattr(schedule, k, v)
    db.session.commit()
    return jsonify(dict(status='ok'))

def schedule_detail(schedule_id):
    params = dict(id=schedule_id)
    schedule = dictfetchone(
        'SELECT * FROM schedules WHERE id = :id',
        params,
    )
    subpieces = {
        table: dictfetchall(
            f'SELECT * FROM {table} WHERE schedule_id = :id',
            params,
        )
        for table in (
            'rotations', 'blocks', 'residents', 'staffing_requirements',
        )
    }

    return {
        **schedule,
        **subpieces,
    }

@bp.route('/schedule/<int:schedule_id>', methods=['GET'])
@login_required
@must_belong_to_schedule
def schedule_detail_view(schedule_id):
    return jsonify(schedule_detail(schedule_id))
