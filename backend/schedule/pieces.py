import itertools
import citrus
from typing import List, Dict, Tuple
from dataclasses import dataclass

@dataclass
class SchedulePieces:
    blocks: List[int]
    rotations: List[int]
    residents: List[int]
    rotations_by_tag: Dict[str, List[int]]
    residents_by_tag: Dict[str, List[int]]

    def vars(self):
        return itertools.product(self.blocks, self.rotations, self.residents)

ScheduleVariables = Dict[Tuple[str, str, str], citrus.Variable]
