from errors import ApiError

class ScheduleError(ApiError):
    pass

class InvalidPreferenceError(ScheduleError):
    pass

class InvalidConstraintError(ScheduleError):
    pass