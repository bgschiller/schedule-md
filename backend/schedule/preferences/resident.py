import citrus
import abc
from functools import reduce
from .base import BasePreference, ScheduleVariables
from .fields import RotationChoice, BlockChoice
from .utils import avg
from ..errors import InvalidPreferenceError
from ..pieces import SchedulePieces

class NoTwoXInARow(BasePreference):
    def __init__(self, resident_id, **rotation_args):
        self.rotations = RotationChoice(**rotation_args)
        self.resident_id = resident_id
    
    def to_json(self):
        return dict(
            resident_id=self.resident_id,
            **self.rotations.to_json(),
        )
    
    def validate(self, schedule: SchedulePieces):
        if self.resident_id not in schedule.residents:
            raise InvalidPreferenceError('Unable to find resident with id {}'.format(self.resident_id))

        self.rotations.validate(schedule)

    def to_affine_expression(self, X: ScheduleVariables, schedule: SchedulePieces):
        rots = self.rotations.resolve(schedule)
        blocks = schedule.blocks
        two_in_a_row = []

        for b1, b2 in zip(blocks, blocks[1:]):
            rotation_in_b1 = reduce(citrus.logical_or, [X[b1, r, self.resident_id] for r in rots])
            rotation_in_b2 = reduce(citrus.logical_or, [X[b2, r, self.resident_id] for r in rots])

            two_in_a_row.append(citrus.negate(rotation_in_b1 & rotation_in_b2))
        return avg(two_in_a_row)

class XDuringY(BasePreference):
    def __init__(self, resident_id, **choice_args):
        self.rotations = RotationChoice(**choice_args)
        self.blocks = BlockChoice(**choice_args)
        self.resident_id = resident_id

    def to_json(self):
        return dict(
            resident_id=self.resident_id,
            **self.blocks.to_json(),
            **self.rotations.to_json(),
        )
    
    def validate(self, schedule: SchedulePieces):
        if self.resident_id not in schedule.residents:
            raise InvalidPreferenceError('Unable to find resident with id {}'.format(self.resident_id))
        self.blocks.validate(schedule)        
        self.rotations.validate(schedule)

    def to_affine_expression(self, X: ScheduleVariables, schedule: SchedulePieces):
        return avg([
            X[block, rotation, self.resident_id]
            for block in self.blocks.resolve(schedule)
            for rotation in self.rotations.resolve(schedule)
        ])

class AsTimelyAsPossible(BasePreference):
    def __init__(self, resident_id, **choice_args):
        self.rotations = RotationChoice(**choice_args)
        self.resident_id = resident_id

    def to_json(self):
        return dict(
            resident_id=self.resident_id,
            **self.rotations.to_json(),
        )

    def validate(self, schedule: SchedulePieces):
        if self.resident_id not in schedule.residents:
            raise InvalidPreferenceError('Unable to find resident with id {}'.format(self.resident_id))
        self.rotations.validate(schedule)

    @abc.abstractmethod
    def get_weights(divisor, num_blocks):
        raise NotImplementedError('to be done by subclasses')

    def to_affine_expression(self, X: ScheduleVariables, schedule: SchedulePieces):
        rots = self.rotations.resolve(schedule)
        divisor = (len(schedule.blocks) - 1) * len(rots)
        weights = self.get_weights(divisor, len(schedule.blocks))
        return sum(
            w * X[b, r, self.resident_id]
            for w, b in zip(weights, schedule.blocks)
            for r in rots
        )

class AsEarlyAsPossible(AsTimelyAsPossible):
    def get_weights(self, divisor, num_blocks):
        return [w / divisor for w in reversed(range(num_blocks))]

class AsLateAsPossible(AsTimelyAsPossible):
    def get_weights(self, divisor, num_blocks):
        return [w / divisor for w in range(num_blocks)]
