from ..pieces import SchedulePieces
from ..errors import InvalidPreferenceError

class RotationChoice:
    def __init__(self, rotation_ids=None, rotation_tag=None, **ignored):
        errors = []
        if rotation_ids is not None and rotation_tag is not None:
            errors.append("May only set one of rotation_ids and rotation_tag")
        if rotation_ids is None and rotation_tag is None:
            errors.append("Must set one of rotation_ids and rotation_tag")

        if errors:
            raise InvalidPreferenceError(errors)

        self.rotation_ids = rotation_ids
        self.rotation_tag = rotation_tag

    def validate(self, schedule: SchedulePieces):
        if self.rotation_tag and self.rotation_tag not in schedule.rotations_by_tag:
            raise InvalidPreferenceError(
                'Unable to find rotation tag with name {}'.format(self.rotation_tag))

        missing_rotations = self.rotation_ids and set(self.rotation_ids) - set(schedule.rotations)
        if missing_rotations:
            raise InvalidPreferenceError(
                'Unable to find rotation(s) with id: {}'.format(missing_rotations))

    def resolve(self, schedule: SchedulePieces):
        if self.rotation_ids:
            return self.rotation_ids
        else:
            return schedule.rotations_by_tag[self.rotation_tag]

    def to_json(self):
        return dict(
            rotation_ids=self.rotation_ids,
            rotation_tag=self.rotation_tag,
        )

class BlockChoice:
    def __init__(self, block_ids, **ignored):
        self.block_ids = block_ids

    def validate(self, schedule: SchedulePieces):
        missing_blocks = set(self.block_ids) - set(schedule.blocks)
        if missing_blocks:
            raise InvalidPreferenceError('Unable to find block(s) with id: {}'.format(missing_blocks))

    def resolve(self, schedule: SchedulePieces):
        return self.block_ids

    def to_json(self):
        return dict(
            block_ids=self.block_ids,
        )

class ResidentChoice:
    def __init__(self, resident_ids=None, resident_tag=None, **ignored):
        if resident_ids is not None and resident_tag is not None:
            raise InvalidPreferenceError("May only set one of resident_ids and resident_tag")
        if resident_ids is None and resident_tag is None:
            raise InvalidPreferenceError("Must set one of resident_ids and resident_tag")

        self.resident_ids = resident_ids
        self.resident_tag = resident_tag

    def validate(self, schedule: SchedulePieces):
        if self.resident_tag and self.resident_tag not in schedule.residents_by_tag:
            raise InvalidPreferenceError(
                'Unable to find resident tag with name {}'.format(self.resident_tag))

        missing_residents = self.resident_ids and set(self.resident_ids) - set(schedule.residents)
        if missing_residents:
            raise InvalidPreferenceError(
                'Unable to find resident(s) with id: {}'.format(missing_residents))

    def resolve(self, schedule: SchedulePieces):
        if self.resident_ids:
            return self.resident_ids
        else:
            return schedule.residents_by_tag[self.resident_tag]

    def to_json(self):
        return dict(
            resident_ids=self.resident_ids,
            resident_tag=self.resident_tag,
        )
