import abc
from typing import Any, Dict, Tuple
import citrus
from ..errors import InvalidPreferenceError
from ..pieces import SchedulePieces, ScheduleVariables

class BasePreference(abc.ABC):
    @abc.abstractmethod
    def validate(self, schedule: SchedulePieces):
        pass

    @classmethod
    def from_json(cls, obj: Dict[str, Any]):
        return cls(**obj)

    @abc.abstractmethod
    def to_json(self):
        pass
    
    @abc.abstractmethod
    def to_affine_expression(self, X: ScheduleVariables, schedule: SchedulePieces):
        pass

