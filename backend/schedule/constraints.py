import itertools
import citrus
from .pieces import SchedulePieces, ScheduleVariables
from .errors import InvalidConstraintError

def no_time_turners(p: citrus.Problem, X: ScheduleVariables, schedule: SchedulePieces):
    """
    Ensure that no resident may work more than one rotation during any block
    """
    for block, resident in itertools.product(schedule.blocks, schedule.residents):
        p.addConstraint(
            sum(X[block, rotation, resident] for rotation in schedule.rotations) == 1,
            '{} can only do one rotation during {}'.format(resident, block))

def exactly_x_blocks_of_rotation(
        p: citrus.Problem, X: SchedulePieces, schedule: SchedulePieces,
        rotation_id, num
):
    for resident in schedule.residents:
        p.addConstraint(
            sum(X[b, rotation_id, resident] for b in schedule.blocks) == num,
            f"{resident} must do exactly {num} blocks of {rotation_id}",
        )

def at_least_x_blocks_of_rotation(
        p: citrus.Problem, X: SchedulePieces, schedule: SchedulePieces,
        rotation_id, num
):
    for resident in schedule.residents:
        p.addConstraint(
            sum(X[b, rotation_id, resident] for b in schedule.blocks) >= num,
            f"{resident} must do at least {num} blocks of {rotation_id}",
        )

def at_most_x_blocks_of_rotation(
        p: citrus.Problem, X: SchedulePieces, schedule: SchedulePieces,
        rotation_id, num
):
    for resident in schedule.residents:
        p.addConstraint(
            sum(X[b, rotation_id, resident] for b in schedule.blocks) <= num,
            f"{resident} must do at most {num} blocks of {rotation_id}",
        )
