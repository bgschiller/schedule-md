INSERT INTO users(id, name, email) VALUES
(1, 'Brian', 'brian@brian.com'),
(2, 'Heidi', 'heidi@dog.com'),
(3, 'Artemis', 'artemis@cat.com');

SELECT pg_catalog.setval(pg_get_serial_sequence('users', 'id'), (SELECT COALESCE(MAX(id), 0) + 1 FROM users));

INSERT INTO schedules(id, owner_id, name) VALUES
(1, 1, 'Dog and Cat Schedule');

SELECT pg_catalog.setval(pg_get_serial_sequence('schedules', 'id'), (SELECT COALESCE(MAX(id), 0) + 1 FROM schedules));

INSERT INTO rotations(id, schedule_id, name, tags) VALUES
(1, 1, 'Nap on Couch', ARRAY['Nap', 'Living Room']),
(2, 1, 'Nap on Bed', ARRAY['Nap', 'Bedroom']),
(3, 1, 'Snuggle on Couch', ARRAY['Nap', 'Living Room']);

SELECT pg_catalog.setval(pg_get_serial_sequence('rotations', 'id'), (SELECT COALESCE(MAX(id), 0) + 1 FROM rotations));

INSERT INTO blocks(id, schedule_id, start_day, end_day, name) VALUES
(1, 1, '2018-01-01', '2018-01-31', 'January'),
(2, 1, '2018-02-01', '2018-02-28', 'February'),
(3, 1, '2018-03-01', '2018-03-30', 'March');

SELECT pg_catalog.setval(pg_get_serial_sequence('blocks', 'id'), (SELECT COALESCE(MAX(id), 0) + 1 FROM blocks));

INSERT INTO staffing_requirements(schedule_id, rotation_id, block_id, at_most, at_least) VALUES
-- Nap on Couch staffing
(1, 1, 1, 0, 0), -- January is too cold
(1, 1, 2, 0, 1), -- February 0 or 1 napper
(1, 1, 3, 1, 1), -- March exactly 1 napper
-- Nap on Bed staffing
(1, 2, 1, 0, 1),
(1, 2, 2, 0, 1),
(1, 2, 3, 0, NULL),
-- Snuggle on Couch
(1, 3, 1, 1, 1),
(1, 3, 2, 0, NULL),
(1, 3, 3, 0, NULL);

INSERT INTO residents(id, user_id, schedule_id, tags) VALUES
(1, 2, 1, ARRAY['dog', 'pet']),
(2, 3, 1, ARRAY['cat', 'pet']);
