import os

MAIL_SERVER = os.getenv('MAIL_SERVER', 'localhost')
MAIL_PORT = os.getenv('MAIL_PORT', '8025')
MAIL_DEFAULT_SENDER = os.getenv('MAIL_DEFAULT_SENDER', 'brian@brianschiller.com')

SQLALCHEMY_DATABASE_URI = os.getenv('DATABASE_URL', 'postgres://localhost/schedule_md')
SQLALCHEMY_TRACK_MODIFICATIONS = False

SECRET_KEY = os.getenv('FLASK_SECRET_KEY', 'moose-socks')

DEBUG = True
