import emptyPromise from '@/utilities/emptyPromise';
import { shallowMount, mount, createLocalVue } from '@vue/test-utils';
import * as api from '@/api';
import Login from '@/views/Login.vue';
jest.mock('@/api');

const localVue = createLocalVue();

describe('Login.vue', () => {
  it('calls sendLoginEmail api call', async () => {
    const wrapper = shallowMount(Login, { localVue });
    wrapper.find('input[type="email"]').setValue('test@test.com');
    wrapper.find('button').trigger('click');
    expect(api.sendLoginEmail).toHaveBeenCalledWith('test@test.com');
  });

  it('displays success when appropriate', async () => {
    const wrapper = mount(Login, { localVue });
    wrapper.setData({
      sendEmailP: Promise.resolve(),
    });
    await localVue.nextTick();
    expect(wrapper.find('[data-success]').exists()).toBe(true);
  });

  it('displays loading when appropriate', async () => {
    const wrapper = mount(Login, { localVue });
    const p = emptyPromise();
    wrapper.setData({
      sendEmailP: p,
    });
    await localVue.nextTick();
    expect(wrapper.find('[data-loading]').exists()).toBe(true);
    p.resolve();
    await localVue.nextTick();
    expect(wrapper.find('[data-loading]').exists()).toBe(false);
  });
});
