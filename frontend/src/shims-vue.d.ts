declare module '*.vue' {
  import Vue from 'vue';
  export default Vue;
}

declare module 'vue-await' {
  import Vue from 'vue';
  export default Vue;
}