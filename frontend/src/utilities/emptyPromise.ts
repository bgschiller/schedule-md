export interface EmptyPromise<T> extends Promise<T> {
  pending?: boolean;
  resolved?: boolean;
  rejected?: boolean;
  resolve: (...args: any) => void;
  reject: (...args: any) => void;
}

export default function emptyPromise(): EmptyPromise<any> {
  let res: (...args: any) => void;
  let rej: (...args: any) => void;
  const p = new Promise((resolve, reject) => {
    res = resolve;
    rej = reject;
  });
  return Object.assign(p, {
    pending: true,
    resolved: undefined,
    rejected: undefined,
    resolve(this: EmptyPromise<any>, ...args: any) {
      res(...args);
      this.resolved = true;
      this.rejected = false;
      this.pending = false;
    },
    reject(this: EmptyPromise<any>, ...args: any) {
      rej(...args);
      this.rejected = true;
      this.resolved = false;
      this.pending = false;
    },
  });
}
