import emptyPromise, { EmptyPromise } from '@/utilities/emptyPromise';

interface PromissoryMock extends jest.Mock<any> {
  p ?: EmptyPromise<any>;
}

function mockWithPromise() {
  const fn = jest.fn() as PromissoryMock;
  fn.mockImplementation(() => {
    sendLoginEmail.p = emptyPromise();
    return sendLoginEmail.p;
  });
  return fn;
}


export const sendLoginEmail = mockWithPromise();

