import Vue from 'vue';
import Vuex, { Store } from 'vuex';

Vue.use(Vuex);

// export interface StoreState {
// }

export default new Vuex.Store({
  modules: {
  },
}); // as Store<StoreState>;
