interface FarfetchdResp {
  resp: Response;
  json: any;
}

interface FarfetchdError {
  status: number;
  statusText: string;
  data: any;
}

interface FarfetchedRequestInit extends RequestInit {
  json?: any;
}

function farfetchd(input: RequestInfo, init?: FarfetchedRequestInit): Promise<FarfetchdResp> {
  if (init && init.json) {
    init.body = JSON.stringify(init.json);
    init.headers = Object.assign(init.headers || {}, {
      'content-type': 'application/json',
    });
  }
  return fetch(input, init)
    .then(async (resp: Response) => {
      if (resp.ok) { return { resp, json: await resp.json() }; }
      return Promise.reject({ status: resp.status, statusText: resp.statusText, data: await resp.json() });
    });
}

export async function sendLoginEmail(email: string): Promise<void> {
  await farfetchd('/api/send_login_email', {
    method: 'POST',
    json: { email },
  });
}

interface BasicUserInfo {
  id: number;
  email: string;
  name: string;
}

export function exerciseLoginToken(token: string): Promise<BasicUserInfo> {
  return farfetchd('/api/login', {
    method: 'POST',
    json: { token },
  })
    .then((resp) => resp.json);
}
