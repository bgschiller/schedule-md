import Vue from 'vue';
import { sync } from 'vuex-router-sync';
import VueRouter from 'vue-router';
import App from './App.vue';
import router from './router';
import store from './store';

Vue.config.productionTip = false;

sync(store, router, { moduleName: 'Route' });

Vue.use(VueRouter);


new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
